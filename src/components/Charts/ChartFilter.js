import React from 'react';
import { FormControl, Select, MenuItem } from '@mui/material';

const tableHeaders = [
  { id: 'location', label: 'Location' },
  { id: 'source', label: 'Source' },
  { id: 'cause', label: 'Cause' },
  { id: 'injured', label: 'Injured' },
  { id: 'reference', label: 'Reference' },
];

const ChartFilter = ({ chart, index, filterOptions, handleFilterChange }) => {
  return (
    <>
      {tableHeaders.map((header) => (
        <FormControl
          required
          variant="outlined"
          fullWidth
          sx={{
            width: '15%',
            height: 'auto',
            backgroundColor: '#E3E8F7',
            borderRadius: '20px',
            '& .MuiOutlinedInput-root': {
              '& fieldset': {
                border: 'none',
              },
              '&:hover fieldset': {
                border: 'none',
              },
              '&.Mui-focused fieldset': {
                border: 'none',
              },
            },
          }}>
          <Select
            displayEmpty
            value={chart.filters[header.id]}
            onChange={(e) => handleFilterChange(index, header.id, e.target.value)}
            inputProps={{ 'aria-label': 'Without label' }}
            renderValue={
              chart.filters[header.id] !== '' ? undefined : () => <span>{header.id}</span>
            }
            sx={{ padding: 0 }}>
            <MenuItem value="">
              <em>{header.id}</em>
            </MenuItem>
            {filterOptions[header.id]?.map((option) => (
              <MenuItem value={option} key={option}>
                {option}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      ))}
    </>
  );
};

export default ChartFilter;
