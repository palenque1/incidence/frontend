import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:8011/api', // Replace with your API base URL
  // baseURL: 'https://incidencias.pgroup.tech/api', 
  // You can set more default options here
});

export default api;
